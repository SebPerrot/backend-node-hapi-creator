#!/bin/bash

rootPath=$1
projectName=$2
localPort=$3
remotePort=$4
version=$5
nodeVersion=$6
rootFilename='server'

docker_compose_file_name="docker-compose.${projectName}-server.yml"
initial="${rootPath:0:1}"
if [ $initial != "/" ]; then
  rootPath="$PWD/$rootPath"
fi

# Create root folder
mkdir $rootPath
cd $rootPath
mkdir $rootFilename
mkdir docker
cd $rootFilename

# Create dockerfile for node
cat > package-lock.json << EOF
EOF

cat > Dockerfile << EOF
FROM node:$nodeVersion

WORKDIR /usr/src/tmp

RUN npm i npm@latest -g

COPY ./package.json /usr/src/tmp/
COPY ./package-lock.json /usr/src/tmp/


RUN npm install
WORKDIR /usr/src/api

COPY ./entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/entrypoint.sh
EOF

cat > entrypoint.sh << EOF
#!/bin/bash
cp -r /usr/src/tmp/* /usr/src/api
npm start
EOF

cd ../docker

# Create .env
cat > .env << EOF
NODE_ENV=development
NODE_PORT=$remotePort
EOF

# Create docker-compose.yml
cat > $docker_compose_file_name << EOF
version: '3.7'

services:
  node:
    build:
      context: '../server'
    environment:
      NODE_ENV: \${NODE_ENV}
      NODE_PORT: \${NODE_PORT}
    container_name: "server-$projectName"
    volumes:
    - ./../$rootFilename:/usr/src/api:cached
    - ./../$rootFilename/node_modules:/usr/src/api/node_modules
    ports:
    - $localPort:$remotePort
    entrypoint: entrypoint.sh
networks:
  default:
EOF

# Initialize package.json
cd $rootPath/$rootFilename
npm init
sleep 2
npx npm-add-script -k start -v "nodemon index.js"

# Install node dependencies
npx npm-add-dependencies nodemon --dev
npx npm-add-dependencies @hapi/hapi @hapi/topo @hapi/hoek @hapi/accept @hapi/ammo @hapi/heavy @hapi/catbox-memory @hapi/bounce @hapi/oppsy @hapi/nigel @hapi/b64 @hapi/statehood @hapi/iron @hapi/cryptiles @hapi/bourne @hapi/pez @hapi/wreck @hapi/content @hapi/shot @hapi/topo @hapi/teamwork @hapi/subtext @hapi/catbox @hapi/mimos @hapi/podium @hapi/somever @hapi/inert @hapi/call @hapi/vise glob path @hapi/vision winston @hapi/good @hapi/boom @hapi/joi hapi-swagger

# Create index.js

cat > index.js << EOF
'use strict';

const Hapi = require("@hapi/hapi");
const path = require("path");
const glob = require("glob");
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');

const logger = require("./utils/logger");

const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || '0.0.0.0';
const REFERENTIAL = require(path.resolve(__dirname, 'package.json'));

const GLOBAL_PREFIX = "api/v$version"

const server = Hapi.server(
    {
      port: PORT,
      host: HOST,
      router: { isCaseSensitive: false, stripTrailingSlash: true }
    }
);

const swaggerOptions = {
  info: {
    title: \`\${REFERENTIAL.name} Documentation\`,
    version: REFERENTIAL.version,
  },
};

const start = async () => {
  await server.register([
      Inert,
      Vision,
      {
          plugin: HapiSwagger,
          options: swaggerOptions
      }
  ]);

  await server.start();
  logger.info(\`Server running at: \${server.info.uri}\`);
  logger.info(\`Documentation living at: \${server.info.uri}/documentation\`);

  const routeFiles = await new Promise((resolve, reject) =>
    glob(
      path.resolve(__dirname, "routes", "**", "!(*.spec).js"),
      (err, files) => (err ? reject(err) : resolve(files)),
    ),
  );

  routeFiles.forEach(file => {
    const { prefix, routes } = require(file);
    routes.map(route => {
      route.config.pre = (route.config.pre || []);
      if (process.env.NODE_ENV != "production" && route.config.validate) {
        route.config.validate.failAction = (handler, h, err) => {
          logger.debug(err.stack);
          throw err;
        };
      }
      return server.route(
        Object.assign(route, {
          path: \`/\${GLOBAL_PREFIX}/\${prefix}\${
            route.path === "/" ? "" : route.path
          }\`,
          config: Object.assign(route.config, {
            tags: ["api", prefix],
          }),
        }),
      );
    });
  });

  return server;
};

process.on("unhandledRejection", err => {
  logger.error("Unhandled exception (not exiting!)");
  logger.error(err.stack);
});

start();
EOF

mkdir utils
cd utils

cat > logger.js << EOF
const winston = require("winston");
const { combine, colorize, timestamp, printf } = winston.format;

let logger;
const logFormat = printf(info => {
  return \`\${info.timestamp} [\${info.level}] \${info.message}\`;
});
logger = winston.createLogger({
  level: "info",
  transports: [
    new winston.transports.Console({
      format: combine(colorize(), timestamp(), logFormat),
    }),
  ],
});

module.exports = logger;
EOF

cd $rootPath/$rootFilename
mkdir routes
cd routes
mkdir v$version
cd v$version

cat > user.route.js << EOF
// Example !
const Joi = require("@hapi/joi");
const Controller = require.main.require("./controllers/user.controller");

module.exports = {
    prefix: "users",
    routes: [
        {
            path: "/",
            method: "GET",
            config: {
                description: "Get all users",
                pre: [],
                validate: {
                    query: {
                        limit: Joi.number().default(10)
                    },
                },
            },
            handler: Controller.getAll,
        },
    ],
};
EOF

cd $rootPath/$rootFilename
mkdir controllers
cd controllers

cat > user.controller.js << EOF
// Example !

module.exports.getAll = async (handler, h) => {
    console.log(handler.query);
    return {
        users: [{ id: 1, username: 'test' }]
    };
}
EOF

dc="docker-compose -f ${docker_compose_file_name}"

cd $rootPath/docker
$dc build --no-cache
$dc up -d

sleep 5
# $dc logs -f

exit

# /Users/sebastienperrot/Documents/Development/Saegus/bic server bic-app 8080 3000 0 10.15.3